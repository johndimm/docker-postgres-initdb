# Initializing a postgres database for docker-compose

These repo creates a postgres instance using docker-compose. Postgres _should_ automatically run the script in the initdb directory, but it doesn't. What's wrong?

It appears that docker is trying to run the initdb script before postgres is started.

## The error

```
$ ./run.sh
Starting postgres-docker-test_db_1 ... done
Attaching to postgres-docker-test_db_1
db_1  | The files belonging to this database system will be owned by user "postgres".
db_1  | This user must also own the server process.
db_1  |
db_1  | The database cluster will be initialized with locale "en_US.utf8".
db_1  | The default database encoding has accordingly been set to "UTF8".
db_1  | The default text search configuration will be set to "english".
db_1  |
db_1  | Data page checksums are disabled.
db_1  |
db_1  | fixing permissions on existing directory /var/lib/postgresql/data ... ok
db_1  | creating subdirectories ... ok
db_1  | selecting dynamic shared memory implementation ... posix
db_1  | selecting default max_connections ... 100
db_1  | selecting default shared_buffers ... 128MB
db_1  | selecting default time zone ... Etc/UTC
db_1  | creating configuration files ... ok
db_1  | running bootstrap script ... ok
db_1  | performing post-bootstrap initialization ... ok
db_1  | syncing data to disk ... ok
db_1  |
db_1  |
db_1  | Success. You can now start the database server using:
db_1  |
db_1  |     pg_ctl -D /var/lib/postgresql/data -l logfile start
db_1  |
db_1  | initdb: warning: enabling "trust" authentication for local connections
db_1  | You can change this by editing pg_hba.conf or using the option -A, or
db_1  | --auth-local and --auth-host, the next time you run initdb.
db_1  | waiting for server to start....2020-12-16 06:38:02.274 UTC [46] LOG:  starting PostgreSQL 13.1 (Debian 13.1-1.pgdg100+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit
db_1  | 2020-12-16 06:38:02.277 UTC [46] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
db_1  | 2020-12-16 06:38:02.301 UTC [47] LOG:  database system was shut down at 2020-12-16 06:37:58 UTC
db_1  | 2020-12-16 06:38:02.327 UTC [46] LOG:  database system is ready to accept connections
db_1  |  done
db_1  | server started
db_1  |
db_1  | /usr/local/bin/docker-entrypoint.sh: running /docker-entrypoint-initdb.d/initdb.sql
db_1  | psql: error: could not connect to server: Connection refused
db_1  | 	Is the server running on host "db" (192.168.0.2) and accepting
db_1  | 	TCP/IP connections on port 5432?

```

docker-compose exits without creating an instance.

## Run again to start the postgres instance

```
$ ./run.sh
Starting postgres-docker-test_db_1 ... done
Attaching to postgres-docker-test_db_1
db_1  | The files belonging to this database system will be owned by user "postgres".
db_1  | This user must also own the server process.
db_1  |
db_1  | The database cluster will be initialized with locale "en_US.utf8".
db_1  | The default database encoding has accordingly been set to "UTF8".
db_1  | The default text search configuration will be set to "english".
db_1  |
db_1  | Data page checksums are disabled.
db_1  |
db_1  | fixing permissions on existing directory /var/lib/postgresql/data ... ok
db_1  | creating subdirectories ... ok
db_1  | selecting dynamic shared memory implementation ... posix
db_1  | selecting default max_connections ... 100
db_1  | selecting default shared_buffers ... 128MB
db_1  | selecting default time zone ... Etc/UTC
db_1  | creating configuration files ... ok
db_1  | running bootstrap script ... ok
db_1  | performing post-bootstrap initialization ... ok
db_1  | syncing data to disk ... ok
db_1  |
db_1  |
db_1  | Success. You can now start the database server using:
db_1  |
db_1  |     pg_ctl -D /var/lib/postgresql/data -l logfile start
db_1  |
db_1  | initdb: warning: enabling "trust" authentication for local connections
db_1  | You can change this by editing pg_hba.conf or using the option -A, or
db_1  | --auth-local and --auth-host, the next time you run initdb.
db_1  | waiting for server to start....2020-12-16 06:53:49.532 UTC [48] LOG:  starting PostgreSQL 13.1 (Debian 13.1-1.pgdg100+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit
db_1  | 2020-12-16 06:53:49.535 UTC [48] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
db_1  | 2020-12-16 06:53:49.561 UTC [49] LOG:  database system was shut down at 2020-12-16 06:53:46 UTC
db_1  | 2020-12-16 06:53:49.592 UTC [48] LOG:  database system is ready to accept connections
db_1  |  done
db_1  | server started
db_1  |
db_1  | /usr/local/bin/docker-entrypoint.sh: running /docker-entrypoint-initdb.d/initdb.sql
db_1  | psql: error: could not connect to server: Connection refused
db_1  | 	Is the server running on host "db" (192.168.0.2) and accepting
db_1  | 	TCP/IP connections on port 5432?
db_1  |
db_1  | PostgreSQL Database directory appears to contain a database; Skipping initialization
db_1  |
db_1  | 2020-12-16 06:53:56.921 UTC [1] LOG:  starting PostgreSQL 13.1 (Debian 13.1-1.pgdg100+1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit
db_1  | 2020-12-16 06:53:56.922 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
db_1  | 2020-12-16 06:53:56.923 UTC [1] LOG:  listening on IPv6 address "::", port 5432
db_1  | 2020-12-16 06:53:56.933 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
db_1  | 2020-12-16 06:53:56.991 UTC [25] LOG:  database system was interrupted; last known up at 2020-12-16 06:53:49 UTC
db_1  | 2020-12-16 06:54:00.059 UTC [25] LOG:  database system was not properly shut down; automatic recovery in progress
db_1  | 2020-12-16 06:54:00.065 UTC [25] LOG:  invalid record length at 0/15CC848: wanted 24, got 0
db_1  | 2020-12-16 06:54:00.066 UTC [25] LOG:  redo is not required
db_1  | 2020-12-16 06:54:00.132 UTC [1] LOG:  database system is ready to accept connections

```

## Verify that the initdb script is installed

After the fail, run again to start the postgres instance. Then use docker exec to check the file setup.

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
f8515aeb2908        postgres            "docker-entrypoint.s…"   12 seconds ago      Up 10 seconds       0.0.0.0:5432->5432/tcp   postgres-docker-test_db_1

$ docker exec -it a4a742d82f3a /bin/bash
root@a4a742d82f3a:/# ls
bin   dev			  docker-entrypoint.sh	home  lib64  mnt  proc	run   srv  tmp	var
boot  docker-entrypoint-initdb.d  etc			lib   media  opt  root	sbin  sys  usr
root@a4a742d82f3a:/# ls *.d
initdb.sql
root@a4a742d82f3a:/# cd *.d
root@a4a742d82f3a:/docker-entrypoint-initdb.d# cat initdb.sql
create table postgres_docker_test as
select now() as date;
```

## Verify that the script failed to run

```
psql (13.1 (Debian 13.1-1.pgdg100+1))
Type "help" for help.

postgres=# \d
Did not find any relations.
postgres=# \q

SELECT 1
root@a4a742d82f3a:/docker-entrypoint-initdb.d# psql
psql (13.1 (Debian 13.1-1.pgdg100+1))
Type "help" for help.

postgres=# \d
List of relations
Schema | Name | Type | Owner
--------+----------------------+-------+----------
public | postgres_docker_test | table | postgres
(1 row)

postgres=# select \* from postgres_docker_test ;
date

---

2020-12-16 06:57:06.353539+00
(1 row)

```
